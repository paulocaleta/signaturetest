﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Crypto = System.Security.Cryptography;

namespace CSharpExample
{
    class Program
    {
        public static string pubKeyPath = "../../../../../../keys/key_pub.pem";
        public static string pvtKeyPath = "../../../../../../keys/key";

        public static string originalMessage =
            "{\"token\":\"SFc5s3ctQj0UmXt7l8ZYhQ73RnOHpukOdQG3cAf06O6VKrgsT2b1bwJYunfNSJNSKEZxRSH_DoZ0l1.dPqKAQ84OowOrY8A1KWBDsL0ZEL8-\",\"request_uuid\":\"164697b4-6619-423a-970b-0f0c0b5a0917\",\"game_id\":1}";

        static void Main(string[] args)
        {
            RsaSignatureVerifier verifier = new RsaSignatureVerifier(pubKeyPath, pvtKeyPath);

            Console.WriteLine("Original Payload: " + originalMessage);
            
            byte[] signature = verifier.Encode(originalMessage);
            
            Console.WriteLine("Encoded Signature: " + Convert.ToBase64String(signature));
            //byte[] msg = Encoding.ASCII.GetBytes("{\"token\":\"SFc5s3ctQj0UmXt7l8ZYhQ73RnOHpukOdQG3cAf06O6VKrgsT2b1bwJYunfNSJNSKEZxRSH_DoZ0l1.dPqKAQ84OowOrY8A1KWBDsL0ZEL8-\",\"request_uuid\":\"164697b4-6619-423a-970b-0f0c0b5a0917\",\"game_id\":1}");
            //byte[] signature = Convert.FromBase64String(
            //    "fp1eYwwOoPj7A1Y2hAeR4vbJW+F7zv6bTiFFh5rPvQFCocCRe6A5dgxbqfFeAJFSJ+LjAZIIKL+zrCWzLVIJEAZB172mNqNZWO/4jd0YywGflT3eO3eK8wzvkgbj98dSCbN4QSuRAAhIv4E4hgFFvI5FoR1APgqoaffIrxah2YAJtH+vcixSIWqU8IiMAUOkE0vvw2MoyhvIEvO4rOOvTQm6qWQ8c9xFhmgFkLXr+i03Gcp8lMHMobHn9DyTx4bB+BJxJfpEjnby40y57oArXX9HqLx2sQTlP53CP9hOxG/D2h3W6QdNtyOEFRZhC7PaQrj/AuPW+OHXz1kMZ1JxGA==");
            bool verificationResult = verifier.Verify(Encoding.ASCII.GetBytes(originalMessage), signature);
            
            
            
            Console.WriteLine(verificationResult);
            
            //verifier.Dispose();


        }
    }

    internal class RsaSignatureVerifier
    {
        private readonly Crypto.RSA _pvtRsa;
        private readonly Crypto.RSA _pubRsa;

        public RsaSignatureVerifier(string publicKeyPath, string privateKeyPath)
        {
            _pvtRsa = Crypto.RSA.Create();

            _pubRsa = Crypto.RSA.Create();

            byte[] pubKey = ReadPemPublicKey(publicKeyPath);

            //byte[] pvtKey = ReadPemPrivateKey(privateKeyPath);

            char[] pkeyBytes = File.ReadAllText(privateKeyPath).ToCharArray();
            

            ReadOnlySpan<char> pkeySpan = pkeyBytes.AsSpan();

            _pvtRsa.ImportFromPem(pkeySpan);
            _pubRsa.ImportSubjectPublicKeyInfo(pubKey, out _);

        }

        public bool Verify(byte[] msg, byte[] signature)
        {
            return _pubRsa.VerifyData(msg, signature, Crypto.HashAlgorithmName.SHA256, Crypto.RSASignaturePadding.Pkcs1);
        }

        public byte[] Encode(string payload)
        {
            byte[] payloadBytes = Encoding.ASCII.GetBytes(payload);
            return _pvtRsa.SignData(payloadBytes, Crypto.HashAlgorithmName.SHA256, Crypto.RSASignaturePadding.Pkcs1);
        }

        private byte[] ReadPemPublicKey(string publicKeyPath)
        {
            string encodedPublicKey = File
                .ReadAllText(publicKeyPath)
                .Replace("-----BEGIN PUBLIC KEY-----", string.Empty)
                .Replace("-----END PUBLIC KEY-----", string.Empty);

            return Convert.FromBase64String(encodedPublicKey);
        }

    }
}