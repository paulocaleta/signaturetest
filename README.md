# Signature Test

This is a template to explain how to use Auth-Signature at Caleta Gaming System.

# Request Signing

For security reasons all **Games API** requests have to be signed by Operator.

Before the integration, the **Operator** generates a **private/public key pair** and sends the **public** key to Caleta.

The body of all requests will be signed with `RSA-SHA256` using the respective private key and encoded to BASE64 (We have a python script example that can be sent over). The signature will be placed in the `X-Auth-Signature` header.

## Python sample.

Python sample uses Pycryptodome-3.9.4 to run.

# Key generation.

First to generate private key

> openssl genrsa -out key.pem 2048

Then, generate the public key

> openssl rsa -in key.pem -outform PEM -pubout -out public.pem
