
'''
 Frst to generate private key
 > openssl genrsa -out key.pem 2048

 Then, generate the public key
 > openssl rsa -in key.pem -outform PEM -pubout -out public.pem
'''

import json
from base64 import b64decode, b64encode

from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15


def verify(msg, key, signature):
    h = SHA256.new(msg)
    verifier = pkcs1_15.new(key)

    try:
        verifier.verify(h, b64decode(signature))
        return True
    except (ValueError, TypeError) as e:
        print(e)
        return False


def encrypt(msg, key):
    h = SHA256.new(msg)
    sign = pkcs1_15.new(key).sign(h)
    return b64encode(sign)


def main():
    public = RSA.import_key(open('../keys/key_pub.pem').read())
    private = RSA.import_key(open('../keys/key').read())

    # Payload Message
    msg = b'{"token": "c6a5bf7d1cc6beaf5acaa130de6e1e69", "request_uuid": "bd794108-6fa5-4ff5-a26e-00afe362a48f", "game_id": 159}'

    # Generates Signature using private key.
    signature = encrypt(msg, private)

    # Prints signature. 
    print(signature)

    # Verifiy payload and signature using public key.
    if verify(msg, public, signature):
        print("Authentic")
    else:
        print("Not Authentic")


if __name__ == "__main__":
    main()
