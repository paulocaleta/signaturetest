/*
First to generate private key
> openssl genrsa -out key.pem 2048

Then, generate the public key
> openssl rsa -in key.pem -outform PEM -pubout -out public.pem
*/

const { readFileSync } = require("node:fs");
const { createSign, createVerify } = require("node:crypto");

function verify(msg, key, signature) {
    const verify = createVerify("SHA256");
    verify.write(msg);
    verify.end();
    return verify.verify(key, signature, "base64");
}

function encrypt(msg, key) {
    const sign = createSign("SHA256");
    sign.write(msg);
    sign.end();
    return sign.sign(key, "base64");
}

const public = readFileSync("keys/key_pub.pem", "utf-8");
const private = readFileSync("keys/key", "utf-8");

// Payload Message
const msg =
    '{"token": "c6a5bf7d1cc6beaf5acaa130de6e1e69", "request_uuid": "bd794108-6fa5-4ff5-a26e-00afe362a48f", "game_id": 159}';

// Generates Signature using private key.
signature = encrypt(msg, private);

// Prints signature.
console.log(signature);

// Verifiy payload and signature using public key.
if (verify(msg, public, signature)) console.log("Authentic");
else console.log("Not Authentic");
