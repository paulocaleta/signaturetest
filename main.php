<?php

/*
  First to generate private key
  > openssl genrsa -out key.pem 2048

  Then, generate public key
  > openssl rsa -in key.pem -outform PEM -pubout -out public.pem
 */


function authSignature($text)
{
    $key = openssl_pkey_get_private('file://keys/key');
    openssl_sign($text, $signature, $key, OPENSSL_ALGO_SHA256);
    $base64 = base64_encode($signature);
    return $base64;
}

function verify($msg, $signature) 
{
    $key = openssl_pkey_get_public('file://keys/key_pub.pem');
    $result = openssl_verify($msg, base64_decode($signature), $key, OPENSSL_ALGO_SHA256);
    return $result;
}

$msg = '{"token": "c6a5bf7d1cc6beaf5acaa130de6e1e69", "request_uuid": "bd794108-6fa5-4ff5-a26e-00afe362a48f", "game_id": 159}';

$signature = authSignature($msg);
print("Signature:\n".$signature."\n");

$result = verify($msg, $signature);
print("validating with public key: ");
if ($result) {
    print("Authentic\n");
} else {
    print("Not Authentic\n");
}
